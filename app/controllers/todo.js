import Ember from 'ember';

function getSetField(model, key, value) {
  if (value === undefined) { // Getter
    return model.get(key);
  }
  else { // Setter
    console.log(`Setting ${key} to ${value}`);
    model.set(key, value);
    console.log('Saving TODO item ' + model.get('title'));
    model.save();
    return value;
  }
}

// NOTE: The todos template (todos.hbs) forces each todo item to be bound to
// this controller on the fly. The resulting loop has a single "todo" variable
// which now points to the *controller*, not the model. Thus, any model
// properties we want to access must be represented in the controller.
export default Ember.Controller.extend({
  actions: {
    editTodo: function() {
      this.set('is_editing', true);
    },
    acceptChanges: function() {
      // First, update the UI.
      this.set('is_editing', false);

      var model = this.get('model');
      if (Ember.isEmpty(model.get('title'))) {
        // No title means delete. this.send() is Ember for "call another
        // action within this action."
        this.send('removeTodo');
      }
      else {
        model.save();
      }
    },
    removeTodo: function() {
      var model = this.get('model');
      // deleteRecord(), like save(), is provided automatically by
      // Ember Data.
      model.deleteRecord();
      model.save();
    }
  },
  is_completed: function(key, value) {
    return getSetField(this.get('model'), key, value);
  }.property('model.is_completed'),

  title: function(key, value) {
    return getSetField(this.get('model'), key, value);
  }.property('model.title')
});
