import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    createTodo: function() {
      var title = this.get('new_title');

      // Bail out if there's nothing (or just whitespace) in the input field
      if (!title.trim()) {
        return;
      }

      // Create the model
      var todo = this.store.createRecord('todo', {
        title: title,
        is_completed: false
      });

      // Save the model
      todo.save();

      // Reset the input field
      this.set('new_title', '');
    },

    clearCompleted: function() {
      var completed = this.get('model').filterBy('is_completed', true);

      // Call the "deleteRecord" action on each of the matching to do models.
      completed.invoke('deleteRecord');
      // Call the "save" action on each of the matching models.
      completed.invoke('save');
    }
  },

  remaining: function() {
    return this.get('model').filterBy('is_completed', false).get('length');

    // Note the change from the blog post. We use "model.@each", not
    // just "@each". That's because automatic controller-to-model
    // proxying has been deprecated (since the time the blog post was written).
  }.property('model.@each.is_completed'),

  // Handle pluralization of this model (i.e., array of TODOs)
  inflection: function() {
    return this.get('remaining') === 1 ? "todo" : "todos";
  }.property('remaining'),

  completed: function() {
    return this.get('model').filterBy('is_completed', true).get('length');
  }.property('model.@each.is_completed'),

  has_completed: function() {
    return this.get('completed') > 0;
  }.property('completed'),

  all_are_completed: function(key, value) {
    var todos = this.get('model');

    if (value === undefined) {
      // NOTE: The JavaScript "!!" operator converts its argument to a boolean.
      // Thus, the expression !!0 returns false, and !!1 returns true. See
      // http://stackoverflow.com/questions/784929/what-is-the-not-not-operator-in-javascript
      //
      // (It's yet another ugly JavaScript idiom.)
      //
      // The original blog article had this:
      //
      // return !!this.get('length') && this.isEvery('is_completed')
      //
      // With the latest Ember, that really needs to be:
      //
      // var todos = this.get('model');
      // return !!todos.length && todos.isEvery('is_completed');
      //
      // That statement relies on both truthiness and the !! conversion.
      //
      // Yuck.
      //
      // This reads better:

      return (todos.get('length') > 0) && todos.isEvery('is_completed');
    }
    else { // Setter
      todos.setEach('is_completed', value);
      todos.invoke('save');
      return value;
    }
  }.property('model.@each.is_completed')
});
