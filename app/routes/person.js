import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
    console.log(`getting person with ID ${params.id}`);
    return {
      person: {
        "id":   params.id,
        "firstName": "John",
        "lastName":  "Bigbootie",
        "age":        50,
        "salary":     0
      }
    };
  }
});
