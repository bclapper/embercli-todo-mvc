import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    // Get only active todos. We do that by filtering all todo's by a
    // function.
    return this.store.filter('todo', function(todo) {
      return todo.get('is_completed');
    });
  },
  // In this route, we want to render the same index template, but with a
  // different set of To Do items. Using renderTemplate() allows us to do that.
  renderTemplate: function(controller) {
    this.render('todos/index', {
      controller: controller
    });
  }
});
