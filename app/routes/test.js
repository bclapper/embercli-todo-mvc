import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return Ember.$.getJSON("/api/test.json").then(function(data) {
      return {
        firstName: data.first_name,
        lastName:  data.last_name
      };
    });
  }
});
