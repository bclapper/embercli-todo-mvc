// This is the transition map used by Liquid Fire. See
// http://ef4.github.io/liquid-fire/
export default function() {

  // From the test.index route to either test.a or test.b, use the
  // "toUp" transition.
  this.transition(
    this.fromRoute('test.index'),
    this.toRoute(['test.a', 'test.b']),
    this.use('toUp')
  );

  // Between test.a and test.b, slide left and right.
  this.transition(
    this.fromRoute('test.a'),
    this.toRoute('test.b'),
    this.use('toLeft'),
    this.reverse('toRight')
  );


  function isTopLevelRoute(routeName) {
    console.log(`routeName: ${routeName}. Top level? ${routeName.indexOf('.')}`);
    return routeName.indexOf('.') === -1;
  }

  // Use a 1.5-second cross-fade between all top-level routes.
  this.transition(
    // Generic form: Matches all top-level routes. See
    // http://ef4.github.io/liquid-fire/#/transition-map/route-constraints
    this.fromRoute(function(routeName) {
      return isTopLevelRoute(routeName);
    }),
    this.toRoute(function(routeName) {
      return isTopLevelRoute(routeName);
    }),
    this.use('crossFade', {duration: 1500})
  );

}
