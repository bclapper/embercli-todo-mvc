import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  this.route('test', {path: '/test'}, function() {
    this.route("a", {path: "a"});
    this.route("b", {path: "b"});
  });
  this.route('test2', {path: '/test2'});
  this.route('people', {path: "/people"});
  this.resource('person', {path: '/person/:id'});
  this.resource('todos', {path: '/todos'}, function() {
    // Remember: We get the "index" route for free.
    this.route('active');
    this.route('completed');
  });
});
